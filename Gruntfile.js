module.exports = function (grunt) {
    //config description
    grunt.initConfig ({
        pkg: grunt.file.readJSON('package.json'),

        jshint: {
            options: {
                curly: true,
                eqeqeq: true,
                immed: true,
                latedef: true,
                newcap: true,
                noarg: true,
                sub: true,
                undef: true,
                eqnull: true,
                browser: true,
                globals: {
                    angular: true,
                    events: true,
                    requestTypes: true,
                    colors: true,
                    clientGame: true,
                    movementTypes: true,
                    weaponTypes: true
                }
            },
            '<%= pkg.name %>': {
                src: [ 'app/components/**/js/*.js' ]
}
        },

        uglify: {
            build:
             {
                src: ['assets/libs/angular.min.js', 'assets/js/constants.js', 'assets/js/clientGame.js', 'app/components/home/mainController.js', 'app/components/menu/menuController.js', 'app/components/battle/js/battleController.js', 'app/components/battle/js/tankController.js', 'app/components/shop/shopController.js', 'app/components/lobby/js/lobbyController.js', 'app/components/home/webSocketController.js'],
                dest: 'dest/build.min.js'
             }
        },

        cssmin: {
            files:
            {
                src: [ 'assets/css/*.css' ],
                dest: 'dest/build.min.css'
            }
        },

        processhtml: {

            dist: {
                options: {
                    process: true,
                    data: {
                        title: 'My app',
                        message: 'This is production distribution'
                    }
                },
                files: {
                    'dest/index.html': [ 'index.html' ],
                    'dest/app/components/battle/html/battleScreen.html': [ 'app/components/battle/html/battleScreen.html' ],
                    'dest/app/components/battle/html/confirmation.html': [ 'app/components/battle/html/confirmation.html' ],
                    'dest/app/components/battle/html/results.html': [ 'app/components/battle/html/results.html' ],
                    'dest/app/components/lobby/html/lobbyCreate.html': [ 'app/components/lobby/html/lobbyCreate.html' ],
                    'dest/app/components/lobby/html/lobbyRoom.html': [ 'app/components/lobby/html/lobbyRoom.html' ],
                    'dest/app/components/lobby/html/lobbyRoomsList.html': [ 'app/components/lobby/html/lobbyRoomsList.html' ],
                    'dest/app/components/menu/menu.html': [ 'app/components/menu/menu.html' ],
                    'dest/app/components/shop/shop.html': [ 'app/components/shop/shop.html' ]
                }
            }
        },

        copy: {

            images: {
                expand:true,
                cwd: 'assets/img/',
                src: '*.{png,gif,jpg}',
                dest: 'dest/img/'
            },

            fonts: {
                expand:true,
                cwd: 'assets/fonts/',
                src: '*.ttf',
                dest: 'dest/fonts/'
            }
        }
    });

    //Load plugins
    grunt.loadNpmTasks ('grunt-contrib-jshint');
    grunt.loadNpmTasks ('grunt-contrib-uglify');
    grunt.loadNpmTasks ('grunt-contrib-cssmin');
    grunt.loadNpmTasks ('grunt-processhtml');
    grunt.loadNpmTasks('grunt-contrib-copy');

    //Register task
    grunt.registerTask ('default', ['jshint', 'uglify', 'cssmin', 'processhtml', 'copy']);

};