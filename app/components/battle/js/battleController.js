angular.module('UoT').controller('battleController', [ '$scope', function ($scope) {

    $scope.game = {};
    $scope.isUserTurn = true;
    var battleTimerObject = null;

    $scope.OwnerName = $scope.UserName;
    $scope.EnemyName = "Bot";

    //Default value of health points
    $scope.ownerHP = 999;
    $scope.enemyHP = 999;

    //Winner data
    $scope.winnerName = "";
    $scope.winnerColor = "#fff";

    var isPractice = false;
    
    $scope.showHelpMessage = false;

    $scope.showHelp = function () {
        if ($scope.showHelpMessage){
            $scope.showHelpMessage = false;
        }
        else {
            $scope.showHelpMessage = true;
        }
    };

    $scope.isShellChosen = false;
    $scope.isRocketChosen = false;
    $scope.isBombChosen = false;
    $scope.isNuclearChosen = false;

    $scope.weapon = {
        piersingRadius: 3,
        rocketRadius: 5,
        bombRadius: 7,
        nuclearBombRadius: 9,
        piersingDamage: 5,
        rocketDamage: 10,
        bombDamage: 15,
        nuclearBombDamage: 20
    };
    
    $scope.reducePiersing = function() {
        $scope.isShellChosen = true;
        $scope.getPiersing();
    };

    $scope.reduceRocket = function() {
        $scope.weapon.rocket --;
    };

    $scope.reduceBomb = function() {
        $scope.weapon.bomb --;
    };

    $scope.reduceNuclearBomb = function() {
        $scope.weapon.nuclearBomb --;
    };

    var idShell = '';

    $scope.getPiersing = function () {
        if (idShell != ''){
            $scope.borderShellOff(idShell);
        }

        idShell = 'W1';
        $scope.borderShellOn(idShell);

        $scope.shellRadius = $scope.weapon.piersingRadius;
        $scope.shellDamage = $scope.weapon.piersingDamage;
        if ($scope.isUserTurn) {
            var obj = {
                weapon: weaponTypes.piersing,
                game: $scope.game
            };
            $scope.$emit(events.ChangeWeaponEvent, obj);
        }
    };

    $scope.getRocket = function () {
        if (idShell != ''){
            $scope.borderShellOff(idShell);
        }

        idShell = 'W2';
        $scope.borderShellOn(idShell);

        $scope.isShellChosen = true;
        $scope.isRocketChosen = true;
        $scope.isBombChosen = false;
        $scope.isNuclearChosen = false;

        $scope.shellRadius = $scope.weapon.rocketRadius;
        $scope.shellDamage = $scope.weapon.rocketDamage;
        if ($scope.isUserTurn) {
            var obj = {
                weapon: weaponTypes.rocket,
                game: $scope.game
            };
            $scope.$emit(events.ChangeWeaponEvent, obj);
        }
    };

    $scope.getBomb = function () {
        if (idShell != ''){
            $scope.borderShellOff(idShell);
        }

        idShell = 'W3';
        $scope.borderShellOn(idShell);

        $scope.isShellChosen = true;
        $scope.isRocketChosen = false;
        $scope.isBombChosen = true;
        $scope.isNuclearChosen = false;
        
        $scope.shellRadius = $scope.weapon.bombRadius;
        $scope.shellDamage = $scope.weapon.bombDamage;
        if ($scope.isUserTurn) {
            var obj = {
                weapon: weaponTypes.bomb,
                game: $scope.game
            };
            $scope.$emit(events.ChangeWeaponEvent, obj);
        }
    };

    $scope.getNuclearBomb = function () {
        if (idShell != ''){
            $scope.borderShellOff(idShell);
        }

        idShell = 'W4';
        $scope.borderShellOn(idShell);

        $scope.isShellChosen = true;
        $scope.isRocketChosen = false;
        $scope.isBombChosen = false;
        $scope.isNuclearChosen = true;
        
        $scope.shellRadius = $scope.weapon.nuclearBombRadius;
        $scope.shellDamage = $scope.weapon.nuclearBombDamage;

        if ($scope.isUserTurn) {
            var obj = {
                weapon: weaponTypes.nuclearBomb,
                game: $scope.game
            };
            $scope.$emit(events.ChangeWeaponEvent, obj);
        }
    };

    // Shell choose

    addEventListener("keydown", function(event) {
        if (event.keyCode == 49 && $scope.isUserTurn) {
            $scope.reducePiersing();
        }
    });

    addEventListener("keydown", function(event) {
        if (event.keyCode == 50 && $scope.isUserTurn)
        {
            if ($scope.weapon.rocket){
                $scope.getRocket();
            }
        }
    });

    addEventListener("keydown", function(event) {
        if (event.keyCode == 51 && $scope.isUserTurn)
        {
            if ($scope.weapon.bomb){
                $scope.getBomb();
            }
        }
    });

    addEventListener("keydown", function(event) {
        if (event.keyCode == 52 && $scope.isUserTurn)
        {
            if ($scope.weapon.nuclearBomb){
                $scope.getNuclearBomb();
            }
        }
    });

    $scope.goToResult = function () {
        $scope.disableBattle();
        $scope.enableResult();
    };

    $scope.$on(events.ChangeWeaponSuccessEvent, function (event, weaponType) {

        if (!$scope.isUserTurn) {
            $scope.$apply(function () {
                switch (weaponType) {
                    case weaponTypes.piersing:
                        $scope.getPiersing();
                        break;
                    case weaponTypes.rocket:
                        $scope.getRocket();
                        break;
                    case weaponTypes.bomb:
                        $scope.getBomb();
                        break;
                    case weaponTypes.nuclearBomb:
                        $scope.getNuclearBomb();
                        break;
                }
            });
        }
    });

    $scope.$on(events.PlayerQuitEvent, function (event, obj)
    {
        if ($scope.game.playerData[0 ].player === obj.winner)
            $scope.$emit(events.OwnerWinRoundEvent);
        else
            $scope.$emit(events.EnemyWinRoundEvent);
    });

    $scope.$on(events.ShowBattleEvent, function (event, transportObj) {
        $scope.battleCountDown = 60;

        if (battleTimerObject === null) {
            battleTimerObject = setInterval(function () {

                if ($scope.battleCountDown > 0) {
                    $scope.battleCountDown--;
                    $scope.$apply();
                }
                if ($scope.battleCountDown === 0) {
                    $scope.turnEnd();
                }
            }, 1000);
        }

        if (transportObj !== null) {

            $scope.$apply(function () {
                $scope.weapon = transportObj.weapon;
            });

            var data = $scope.game.getPlayerData($scope.UserName);
            data.money = transportObj.money;

            // first turn
            $scope.$apply(changeTurn());
        }
        else {
            isPractice = true;
        }
    });

    $scope.turnEnd = function () {
        if (isPractice) {
            $scope.$apply(changeTurn());
        }
        else {
            $scope.$emit(events.ChangeTurnEvent, $scope.game);
        }
    };

    $scope.$on(events.ChangeTurnSuccessEvent, function (event, player) {
        $scope.$apply(changeTurn());
    });


    $scope.goToConfirmation = function () {
        $scope.disableBattle();
        $scope.enableConfirmation();
    };

    $scope.stayInBattle = function () {
        $scope.enableBattle();
        $scope.disableConfirmation();
    };

    $scope.leaveBattle = function () {
        $scope.disableBattle();
        $scope.disableResult();
        $scope.disableConfirmation();
        $scope.$emit(events.GoToMenuEvent, null);
    };

    $scope.$on(events.CloseResultEvent, function (event, args) {
        $scope.disableResult();
    });

    $scope.$on(events.startGameEventSuccess, function (event, game) {


        $scope.game = new clientGame(game.wind, game.hits, game.rounds);

        for (var i = 0; i < game.playerData.length; i++) {
            $scope.game.addPlayerData(game.playerData[i].player, game.playerData[i].color, game.playerData[i].money);
        }
        $scope.ownerHP = $scope.game.hits;
        $scope.enemyHP = $scope.game.hits;

        console.log(game.wind, game.hits, game.rounds);
    });

    $scope.$on(events.NewReadyPlayerEvent, function (event, player) {
        $scope.game.setReady(player, true);

        // If all players ready => start game
        var i = 0;
        while ($scope.game.playerData[i].ready === true) {
            if (i === $scope.game.playerData.length - 1) {
                $scope.$broadcast(events.AllPlayersReadyEvent, null);
                break;
            }
            i++;
        }
    });

    $scope.$on(events.OwnerWinRoundEvent, function (event, args) {
        if ($scope.game !== undefined) {
            $scope.winnerName = $scope.game.playerData[0].player;
            $scope.winnerColor = $scope.game.playerData[0].color;
        }
        else {
            $scope.winnerName = $scope.UserName;
            $scope.winnerColor = $scope.userColor;
        }
        $scope.goToResult();
    });

    $scope.$on(events.EnemyWinRoundEvent, function (event, args) {
        $scope.winnerName = $scope.game.playerData[1].player;
        $scope.winnerColor = $scope.game.playerData[1].color;
        $scope.goToResult();
    });

    $scope.borderShellOn = function (idShell) {
        var piersingChosen = document.getElementById(idShell);
        piersingChosen.style.border = '0.3vmax solid white';
        return idShell;
    };

    $scope.borderShellOff = function (idShell) {
        var piersingChosen = document.getElementById(idShell);
        piersingChosen.style.border = '';
    };

    function changeTurn() {
        $scope.isShellChosen = false;
        if (idShell != ''){
            $scope.borderShellOff(idShell);
        }
        $scope.game.changeTurn();
        $scope.battleCountDown = 60;
        $scope.isUserTurn = $scope.UserName === $scope.game.getWalkingPlayer();
    }
}
]);