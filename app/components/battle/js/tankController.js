angular.module('UoT').controller('tankController', ['$scope', function ($scope) {

    // Tank Control
    addEventListener("keydown", function(event) {
        if (event.keyCode == 37 && $scope.isUserTurn)
            $scope.moveLeft();
    });

    addEventListener("keydown", function(event) {
        if (event.keyCode == 39 && $scope.isUserTurn)
            $scope.moveRight();
    });

    addEventListener("keydown", function(event) {
        if (event.keyCode == 38 && $scope.isUserTurn)
            $scope.moveUp();
    });

    addEventListener("keydown", function(event) {
        if (event.keyCode == 40 && $scope.isUserTurn)
            $scope.moveDown();
    });

    addEventListener("keydown", function(event) {
        if (event.keyCode == 13 && $scope.isUserTurn)
            $scope.shoot();
    });
    
    var drawCanvas = null;
    var cont = null;

    $scope.userColor = "green";
    $scope.enemyColor = "grey";

    //Ground position
    var groundPaddingX = 0;
    var groundPaddingY = 450;
    var groundLength = 1400;
    var groundWidth = 200;
    var levelPaddingX = 500;
    var levelPaddingY = 350;
    var levelLength = 400;
    var levelWidth = 100;

    var tank = {
        size: 60 //trackLengthPlayer
    };

    //Player's track position
    var trackPaddingXPlayer = tank.size * 3;
    var trackPaddingYPlayer = tank.size * 7.333333333;
    var trackLengthPlayer = tank.size;
    var trackWidthPlayer = tank.size / 5;

    //Enemy's track position
    var trackPaddingXEnemy = tank.size * 18;
    var trackPaddingYEnemy = trackPaddingYPlayer;
    var trackLengthEnemy = trackLengthPlayer;
    var trackWidthEnemy = trackWidthPlayer;

    //Player's front roller position
    var frontRollerPaddingXPlayer = trackPaddingXPlayer + trackLengthPlayer;
    var frontRollerPaddingYPlayer = tank.size * 7.46666666667;
    var frontRollerRadiusPlayer = trackWidthPlayer / 2;
    var frontRollerStartAnglePlayer = 0;
    var frontRollerEndAnglePlayer = Math.PI * 2;

    //Enemy's front roller position
    var frontRollerPaddingXEnemy = trackPaddingXEnemy + trackLengthEnemy;
    var frontRollerPaddingYEnemy = frontRollerPaddingYPlayer;
    var frontRollerRadiusEnemy = frontRollerRadiusPlayer;
    var frontRollerStartAngleEnemy = frontRollerStartAnglePlayer;
    var frontRollerEndAngleEnemy = frontRollerEndAnglePlayer;

    //Player's rear roller position
    var rearRollerPaddingXPlayer = trackPaddingXPlayer;
    var rearRollerPaddingYPlayer = frontRollerPaddingYPlayer;
    var rearRollerRadiusPlayer = frontRollerRadiusPlayer;
    var rearRollerStartAnglePlayer = frontRollerStartAnglePlayer;
    var rearRollerEndAnglePlayer = frontRollerEndAnglePlayer;

    //Enemy's rear roller position
    var rearRollerPaddingXEnemy = trackPaddingXEnemy;
    var rearRollerPaddingYEnemy = rearRollerPaddingYPlayer;
    var rearRollerRadiusEnemy = rearRollerRadiusPlayer;
    var rearRollerStartAngleEnemy = rearRollerStartAnglePlayer;
    var rearRollerEndAngleEnemy = rearRollerEndAnglePlayer;

    //Player's tower position
    var towerPaddingXPlayerR = tank.size * 3.25;
    var towerPaddingYPlayerR = tank.size * 7.11666667;
    var towerLengthPlayerR = tank.size / 2;
    var towerWidthPlayerR = tank.size * 0.21666666;
    var towerPaddingXPlayer = tank.size * 3.7;
    var towerPaddingYPlayer = tank.size * 7.23333333;
    var towerRadiusPlayer = tank.size / 9.5;
    var towerStartAnglePlayer = 0;
    var towerEndAnglePlayer = Math.PI * 2;

    //Enemy's tower position
    var towerPaddingXEnemyR = tank.size * 18.25;
    var towerPaddingYEnemyR = towerPaddingYPlayerR;
    var towerLengthEnemyR = towerLengthPlayerR;
    var towerWidthEnemyR = towerWidthPlayerR;
    var towerPaddingXEnemy = tank.size * 18.28333333;
    var towerPaddingYEnemy = tank.size * 7.23333333;
    var towerRadiusEnemy = towerRadiusPlayer;
    var towerStartAngleEnemy = 0;
    var towerEndAngleEnemy = towerEndAnglePlayer;

    //Player's gun position
    var gunRotationPointXPlayer = tank.size * 3.65;
    //var gunRotationPointYPlayer = tank.size * 7.16666666667;
    var gunRotationPointYPlayer = tank.size * 7.19;
    var gunAnglePlayer = 0;
    var gunEndAnglePlayer = Math.PI / 180;
    var gunPaddingXPlayer = gunAnglePlayer;
    var gunPaddingYPlayer = gunAnglePlayer;
    var gunLengthPlayer = tank.size / 1.714285714285714;
    var gunWidthPlayer = tank.size / 15;

    //Enemy's gun position
    var gunRotationPointXEnemy = towerPaddingXEnemy;
    var gunRotationPointYEnemy = gunRotationPointYPlayer;
    var gunAngleEnemy = 0;
    var gunEndAngleEnemy = gunEndAnglePlayer;
    var gunPaddingXEnemy = gunAnglePlayer;
    var gunPaddingYEnemy = gunAnglePlayer;
    var gunLengthEnemy = -gunLengthPlayer;
    var gunWidthEnemy = gunWidthPlayer;

    // Clear canvas values
    var originToClearX = 0;
    var originToClearY = 0;
    var canvasWidth = 1500;
    var canvasHeight = 700;

    // Spacing
    var moveSpacing = 30;
    var rotationAngleSpacing = 10;

    //Move right values
    var maxTrackPaddingXPlayer = levelPaddingX - tank.size * 2;
    var maxFrontRollerPaddingXPlayer = maxTrackPaddingXPlayer + trackLengthPlayer;
    var maxRearRollerPaddingXPlayer = maxTrackPaddingXPlayer;
    var maxTowerPaddingXPlayer = tank.size * 7;
    var maxTowerPaddingXPlayerR = tank.size * 6.58333333;
    var maxGunRotationPointXPlayer = maxTowerPaddingXPlayer;
    var maxTrackPaddingXEnemy = groundLength - tank.size - trackLengthPlayer;
    var maxFrontRollerPaddingXEnemy = maxTrackPaddingXEnemy;
    var maxRearRollerPaddingXEnemy = groundLength - tank.size;
    var maxTowerPaddingXEnemy = tank.size * 21.61666666;
    var maxTowerPaddingXEnemyR = tank.size * 21.58333333;
    var maxGunRotationPointXEnemy = tank.size * 21.6666666;

    //Move left values
    var minTrackPaddingXPlayer = tank.size;
    var minRearRollerPaddingXPlayer = minTrackPaddingXPlayer;
    var minFrontRollerPaddingXPlayer = minTrackPaddingXPlayer + trackLengthPlayer;
    var minTowerPaddingXPlayer = tank.size * 1.7;
    var minTowerPaddingXPlayerR = tank.size * 1.25;
    var minGunRotationPointXPlayer = tank.size * 1.65;
    var minTrackPaddingXEnemy = levelPaddingX + levelLength + tank.size;
    var minFrontRollerPaddingXEnemy = trackPaddingXEnemy - trackLengthEnemy;
    var minRearRollerPaddingXEnemy = minFrontRollerPaddingXEnemy - trackLengthEnemy;
    var minTowerPaddingXEnemy = tank.size * 16.28333333;
    var minTowerPaddingXEnemyR = tank.size * 16.25;
    var minGunRotationPointXEnemy = minTowerPaddingXEnemy;

    //Gun rotation values
    var maxRotationAnglePlayer = 50;
    var minRotationAnglePlayer = -10;
    var maxRotationAngleEnemy = minRotationAnglePlayer;
    var minRotationAngleEnemy = maxRotationAnglePlayer;

   //Shooting values
    var shellStartXPosition = 0;
    var shellStartYPosition = 0;
    var shotPower = 50;
    var shootAngle = 90 - gunAnglePlayer;
    var shellNextXPosition = 0;
    var shellNextYPosition = 0;
    var shellOldXPosition = 0;
    var shellOldYPosition = 0;

    $scope.$on(events.DrawCanvasEvent, function (event, arg) {

        if ($scope.game !== undefined && $scope.inPractice === false) {

            $scope.$apply(function () {
               $scope.userColor = $scope.game.playerData[ 0 ].color;
               $scope.enemyColor = $scope.game.playerData[ 1 ].color;
                $scope.OwnerName = $scope.game.playerData[ 0 ].player;
                $scope.EnemyName = $scope.game.playerData[ 1 ].player;
           });
        }

        drawCanvas = document.getElementById('tank');
        cont = drawCanvas.getContext('2d');
        $scope.drawField ();
        $scope.drawTanks ();
    });


    $scope.$on(events.PlayerMoveUpdateEvent, function (event, args) {

        var player = args.player;
        var movementType = args.movementType;

        if (player === $scope.game.getPlayers()[0]) {
            switch (movementType) {
                case movementTypes.moveUp:
                    moveUpFirstPlayer();
                    break;
                case movementTypes.moveDown:
                    moveDownFirstPlayer();
                    break;
                case movementTypes.moveLeft:
                    moveLeftFirstPlayer();
                    break;
                case movementTypes.moveRight:
                    moveRightFirstPlayer();
                    break;
                case movementTypes.shoot:
                    firstPlayerShoot();
                    break;
            }
        }
        else {
            switch (movementType) {
                case movementTypes.moveUp:
                    moveUpSecondPlayer();
                    break;
                case movementTypes.moveDown:
                    moveDownSecondPlayer();
                    break;
                case movementTypes.moveLeft:
                    moveLeftSecondPlayer();
                    break;
                case movementTypes.moveRight:
                    moveRightSecondPlayer();
                    break;
                case movementTypes.shoot:
                    secondPlayerShoot();
                    break;
            }
        }
    });

    $scope.drawField = function drawField() {

        cont.beginPath();
        cont.fillStyle = "green";
        cont.fillRect(groundPaddingX, groundPaddingY, groundLength, groundWidth);
        cont.fillRect(levelPaddingX, levelPaddingY, levelLength, levelWidth);
        cont.closePath();
        cont.fill();
    };

    $scope.drawTanks = function drawTanks() {

        cont.beginPath();
        cont.fillStyle = "grey";
        cont.fillRect(trackPaddingXPlayer, trackPaddingYPlayer, trackLengthPlayer, trackWidthPlayer);
        cont.closePath();
        cont.stroke();
        cont.fill();

        //Front roller
        cont.beginPath();
        cont.fillStyle = "darkgrey";
        cont.arc(frontRollerPaddingXPlayer, frontRollerPaddingYPlayer-2, frontRollerRadiusPlayer-2, frontRollerStartAnglePlayer, frontRollerEndAnglePlayer, true);
        cont.fill();
        cont.closePath();

        cont.arc(frontRollerPaddingXPlayer-10, frontRollerPaddingYPlayer+5, frontRollerRadiusPlayer-1, frontRollerStartAnglePlayer, frontRollerEndAnglePlayer, true);
        cont.fill();
        cont.closePath();

        cont.arc(frontRollerPaddingXPlayer-24, frontRollerPaddingYPlayer+5, frontRollerRadiusPlayer-1, frontRollerStartAnglePlayer, frontRollerEndAnglePlayer, true);
        cont.fill();
        cont.closePath();

        cont.arc(frontRollerPaddingXPlayer-38, frontRollerPaddingYPlayer+5, frontRollerRadiusPlayer-1, frontRollerStartAnglePlayer, frontRollerEndAnglePlayer, true);
        cont.fill();
        cont.closePath();

        cont.arc(frontRollerPaddingXPlayer-52, frontRollerPaddingYPlayer+5, frontRollerRadiusPlayer-1, frontRollerStartAnglePlayer, frontRollerEndAnglePlayer, true);
        cont.fill();
        cont.closePath();

        //Rear roller
        cont.arc(rearRollerPaddingXPlayer, rearRollerPaddingYPlayer-2, rearRollerRadiusPlayer-2, rearRollerStartAnglePlayer, rearRollerEndAnglePlayer, true);
        cont.fill();
        cont.closePath();

        cont.beginPath();
        cont.fillStyle = "black";

        cont.arc(frontRollerPaddingXPlayer, frontRollerPaddingYPlayer-2, 1, frontRollerStartAnglePlayer, frontRollerEndAnglePlayer, true);
        cont.fill();
        cont.closePath();

        cont.beginPath();
        cont.arc(frontRollerPaddingXPlayer-10, frontRollerPaddingYPlayer+5, 1.5, frontRollerStartAnglePlayer, frontRollerEndAnglePlayer, true);
        cont.fill();
        cont.closePath();

        cont.beginPath();
        cont.arc(frontRollerPaddingXPlayer-24, frontRollerPaddingYPlayer+5, 1.5, frontRollerStartAnglePlayer, frontRollerEndAnglePlayer, true);
        cont.fill();
        cont.closePath();

        cont.beginPath();
        cont.arc(frontRollerPaddingXPlayer-38, frontRollerPaddingYPlayer+5, 1.5, frontRollerStartAnglePlayer, frontRollerEndAnglePlayer, true);
        cont.fill();
        cont.closePath();

        cont.beginPath();
        cont.arc(frontRollerPaddingXPlayer-52, frontRollerPaddingYPlayer+5, 1.5, frontRollerStartAnglePlayer, frontRollerEndAnglePlayer, true);
        cont.fill();
        cont.closePath();

        cont.beginPath();
        cont.arc(rearRollerPaddingXPlayer, rearRollerPaddingYPlayer-2, 1, rearRollerStartAnglePlayer, rearRollerEndAnglePlayer, true);
        cont.fill();
        cont.closePath();

        //Tower
        cont.beginPath();
        cont.fillStyle = $scope.userColor;
        cont.fillRect(towerPaddingXPlayerR, towerPaddingYPlayerR, towerLengthPlayerR, towerWidthPlayerR);
        cont.arc(towerPaddingXPlayer, towerPaddingYPlayer, towerRadiusPlayer, towerStartAnglePlayer, towerEndAnglePlayer, true);
        cont.fill();

        //Gun
        cont.save();
        cont.beginPath();
        cont.translate(gunRotationPointXPlayer, gunRotationPointYPlayer);
        cont.rotate(-gunAnglePlayer * gunEndAnglePlayer);
        cont.fillRect(gunPaddingXPlayer, gunPaddingYPlayer, gunLengthPlayer, gunWidthPlayer); // padding from the
                                                                                              // rotation point
        cont.fill();
        cont.restore();

        //Enemy's tank
        //Track
        cont.beginPath();
        cont.fillStyle = "grey";
        cont.fillRect(trackPaddingXEnemy, trackPaddingYEnemy, trackLengthEnemy, trackWidthEnemy);
        cont.closePath();
        cont.stroke();
        cont.fill();

        //Front roller
        cont.beginPath();
        cont.fillStyle = "darkgrey";
        cont.arc(frontRollerPaddingXEnemy, frontRollerPaddingYPlayer-2, frontRollerRadiusPlayer-2, frontRollerStartAnglePlayer, frontRollerEndAnglePlayer, true);
        cont.fill();
        cont.closePath();

        cont.arc(frontRollerPaddingXEnemy-10, frontRollerPaddingYPlayer+5, frontRollerRadiusPlayer-1, frontRollerStartAnglePlayer, frontRollerEndAnglePlayer, true);
        cont.fill();
        cont.closePath();

        cont.arc(frontRollerPaddingXEnemy-24, frontRollerPaddingYPlayer+5, frontRollerRadiusPlayer-1, frontRollerStartAnglePlayer, frontRollerEndAnglePlayer, true);
        cont.fill();
        cont.closePath();

        cont.arc(frontRollerPaddingXEnemy-38, frontRollerPaddingYPlayer+5, frontRollerRadiusPlayer-1, frontRollerStartAnglePlayer, frontRollerEndAnglePlayer, true);
        cont.fill();
        cont.closePath();

        cont.arc(frontRollerPaddingXEnemy-52, frontRollerPaddingYPlayer+5, frontRollerRadiusPlayer-1, frontRollerStartAnglePlayer, frontRollerEndAnglePlayer, true);
        cont.fill();
        cont.closePath();

        //Rear roller
        cont.arc(rearRollerPaddingXEnemy, rearRollerPaddingYPlayer-2, rearRollerRadiusPlayer-2, rearRollerStartAnglePlayer, rearRollerEndAnglePlayer, true);
        cont.fill();
        cont.closePath();

        cont.beginPath();
        cont.fillStyle = "black";

        cont.arc(frontRollerPaddingXEnemy, frontRollerPaddingYPlayer-2, 1, frontRollerStartAnglePlayer, frontRollerEndAnglePlayer, true);
        cont.fill();
        cont.closePath();

        cont.beginPath();
        cont.arc(frontRollerPaddingXEnemy-10, frontRollerPaddingYPlayer+5, 1.5, frontRollerStartAnglePlayer, frontRollerEndAnglePlayer, true);
        cont.fill();
        cont.closePath();

        cont.beginPath();
        cont.arc(frontRollerPaddingXEnemy-24, frontRollerPaddingYPlayer+5, 1.5, frontRollerStartAnglePlayer, frontRollerEndAnglePlayer, true);
        cont.fill();
        cont.closePath();

        cont.beginPath();
        cont.arc(frontRollerPaddingXEnemy-38, frontRollerPaddingYPlayer+5, 1.5, frontRollerStartAnglePlayer, frontRollerEndAnglePlayer, true);
        cont.fill();
        cont.closePath();

        cont.beginPath();
        cont.arc(frontRollerPaddingXEnemy-52, frontRollerPaddingYPlayer+5, 1.5, frontRollerStartAnglePlayer, frontRollerEndAnglePlayer, true);
        cont.fill();
        cont.closePath();

        cont.beginPath();
        cont.arc(rearRollerPaddingXEnemy, rearRollerPaddingYPlayer-2, 1, rearRollerStartAnglePlayer, rearRollerEndAnglePlayer, true);
        cont.fill();
        cont.closePath();

        //Tower
        cont.beginPath();
        cont.fillStyle = $scope.enemyColor;
        cont.fillRect(towerPaddingXEnemyR, towerPaddingYEnemyR, towerLengthEnemyR, towerWidthEnemyR);
        cont.arc(towerPaddingXEnemy, towerPaddingYEnemy, towerRadiusEnemy, towerStartAngleEnemy, towerEndAngleEnemy, true);
        cont.fill();

        //Gun
        cont.save();
        cont.beginPath();
        cont.translate(gunRotationPointXEnemy, gunRotationPointYEnemy);
        cont.rotate(gunAngleEnemy * gunEndAngleEnemy);
        cont.fillRect(gunPaddingXEnemy, gunPaddingYEnemy, gunLengthEnemy, gunWidthEnemy); // padding from the rotation
                                                                                          // point
        cont.fill();
        cont.restore();
    };

    var shellFly = function shellFly (shellStartXPosition, shellStartYPosition, shellNextXPosition, shellNextYPosition) {

        cont.beginPath();
        cont.moveTo(shellStartXPosition, shellStartYPosition);
        cont.arc(shellNextXPosition, shellNextYPosition, $scope.shellRadius, 0, Math.PI * 2, true);
        cont.fillStyle="black";
        cont.fill();
        cont.closePath();
    };

    $scope.enemyHitSound = function () {
        var audio = new Audio();
        audio.src = '1.mp3';
        audio.autoplay = true;
    };
    
    $scope.ownerHitSound = function () {
        var audio = new Audio();
        audio.src = '2.mp3';
        audio.autoplay = true;
    };

    $scope.enemyTankDamage = function () {
        $scope.enemyHP = $scope.enemyHP - $scope.shellDamage;
        if ($scope.enemyHP <= 0) {
            $scope.$emit(events.OwnerWinRoundEvent);
        }
        if($scope.isUserTurn){
            $scope.ownerHitSound();
        }
        if(!$scope.isUserTurn){
            $scope.enemyHitSound();
        }
    };

    $scope.ownerTankDamage = function () {
        $scope.ownerHP = $scope.ownerHP - $scope.shellDamage;
        if ($scope.ownerHP <= 0) {
            $scope.$emit(events.EnemyWinRoundEvent);
        }
        if($scope.isUserTurn){
            $scope.ownerHitSound();
        }
        if(!$scope.isUserTurn){
            $scope.enemyHitSound();
        }
    };

    var drawBullet = function () {

        $scope.clearMap ();
        $scope.drawField ();
        $scope.drawTanks ();

        shellNextXPosition = shellStartXPosition + (shotPower * Math.sin(shootAngle * Math.PI / 180));
        shellNextYPosition = shellStartYPosition - (shotPower * Math.cos(shootAngle * Math.PI / 180));

        shellFly(shellStartXPosition, shellStartYPosition, shellNextXPosition, shellNextYPosition);

        shootAngle = shootAngle + 5;
        shellOldXPosition = shellStartXPosition;
        shellOldYPosition = shellStartYPosition;

        shellStartXPosition = shellNextXPosition;
        shellStartYPosition = shellNextYPosition;

        console.log("1" + trackLengthEnemy);

        if ((shellStartYPosition > towerPaddingYEnemy-towerRadiusEnemy) && (shellStartXPosition > trackPaddingXEnemy) && (shellStartXPosition < trackPaddingXEnemy+trackLengthEnemy)) {

            console.log(shellStartXPosition + "" + trackPaddingXEnemy + "" + trackLengthEnemy);

            $scope.$apply ( function () {
                $scope.enemyTankDamage();
            });

            return;
        }

        if ((shellStartYPosition < groundPaddingY) && (shellStartXPosition < levelPaddingX || shellStartXPosition > levelPaddingX+levelLength || shellStartYPosition < levelPaddingY)){

            window.requestAnimationFrame(drawBullet);
        }
    };

    var drawBulletEnemy = function () {

        $scope.clearMap ();
        $scope.drawField ();
        $scope.drawTanks ();

        shellNextXPosition = shellStartXPosition - shotPower * Math.sin(shootAngle * Math.PI / 180);
        shellNextYPosition = shellStartYPosition - shotPower * Math.cos(shootAngle * Math.PI / 180);

        shellFly(shellStartXPosition, shellStartYPosition, shellNextXPosition, shellNextYPosition);

        shootAngle = shootAngle + 5;
        shellOldXPosition = shellStartXPosition;
        shellOldYPosition = shellStartYPosition;

        shellStartXPosition = shellNextXPosition;
        shellStartYPosition = shellNextYPosition;

        if (shellStartYPosition > towerPaddingYPlayer-towerRadiusPlayer && shellStartXPosition > trackPaddingXPlayer && shellStartXPosition < trackPaddingXPlayer+trackLengthPlayer) {

            $scope.$apply ( function () {
                $scope.ownerTankDamage();
            });

            return;
        }

        if ((shellStartYPosition < groundPaddingY) && (shellStartXPosition < levelPaddingX || shellStartXPosition > levelPaddingX+levelLength || shellStartYPosition < levelPaddingY)){

            window.requestAnimationFrame(drawBulletEnemy);
        }
    };

    function firstPlayerShoot () {

        $scope.clearMap ();
        $scope.drawField ();
        $scope.drawTanks ();

        shellStartXPosition = gunRotationPointXPlayer;
        shellStartYPosition = gunRotationPointYPlayer;

        shotPower = 50;
        shellNextXPosition = 0;
        shellNextYPosition = 0;
        shootAngle = 90 - gunAnglePlayer;

        window.requestAnimationFrame(drawBullet);

    }

    function secondPlayerShoot () {

        $scope.clearMap ();
        $scope.drawField ();
        $scope.drawTanks ();

        shellStartXPosition = gunRotationPointXEnemy;
        shellStartYPosition = gunRotationPointYEnemy;

        shotPower = 50;
        shellNextXPosition = 0;
        shellNextYPosition = 0;
        shootAngle = 90 - gunAngleEnemy;

        window.requestAnimationFrame(drawBulletEnemy);

        if($scope.inPractice) {
            $scope.isUserTurn = true;
            $scope.$apply ();
        }
    }

    $scope.clearMap = function clearMap() {
        cont.clearRect(originToClearX, originToClearY, canvasWidth, canvasHeight);
    };

    function moveLocalPlayer (movement) {

        if ($scope.game !== undefined && $scope.inPractice === false) {
            var obj = {
                movementType: movement,
                game: $scope.game
            };

            $scope.$emit(events.PlayerMoveEvent, obj);
        }
        else {
            switch (movement) {
                case movementTypes.moveUp:
                    moveUpFirstPlayer();
                    break;
                case movementTypes.moveDown:
                    moveDownFirstPlayer();
                    break;
                case movementTypes.moveLeft:
                    moveLeftFirstPlayer();
                    break;
                case movementTypes.moveRight:
                    moveRightFirstPlayer();
                    break;
                case movementTypes.shoot:
                    firstPlayerShoot();
                    break;
            }
        }
    }

    $scope.moveLeft = function () {
        moveLocalPlayer (movementTypes.moveLeft);
    };

    $scope.moveRight = function () {
       moveLocalPlayer (movementTypes.moveRight);
    };

    $scope.moveUp = function () {
        moveLocalPlayer (movementTypes.moveUp);
    };

    $scope.moveDown = function () {
        moveLocalPlayer (movementTypes.moveDown);
    };

    $scope.chooseShell = "";
    $scope.showChooseShell = false;

    $scope.compTurns = function() {

        var randomNumber = Math.random();

        if (randomNumber < 0.4 || $scope.enemyHP < $scope.ownerHP) {
            setTimeout(moveLeftSecondPlayer, 1000);
            setTimeout(moveUpSecondPlayer, 2000);
            setTimeout(moveUpSecondPlayer, 2500);
            setTimeout(moveUpSecondPlayer, 3000);
            setTimeout(moveUpSecondPlayer, 3500);
            setTimeout(secondPlayerShoot, 4000);
            setTimeout(moveDownSecondPlayer, 4100);
            setTimeout(moveDownSecondPlayer, 4200);
            setTimeout(moveDownSecondPlayer, 4300);
            setTimeout(moveDownSecondPlayer, 4400);

        }
        else {
            setTimeout(moveRightSecondPlayer, 1000);
            setTimeout(moveUpSecondPlayer, 2000);
            setTimeout(moveUpSecondPlayer, 2500);
            setTimeout(moveUpSecondPlayer, 3000);
            setTimeout(moveUpSecondPlayer, 3500);
            setTimeout(moveUpSecondPlayer, 4000);
            setTimeout(secondPlayerShoot, 4500);
            setTimeout(moveDownSecondPlayer, 4600);
            setTimeout(moveDownSecondPlayer, 4700);
            setTimeout(moveDownSecondPlayer, 4800);
            setTimeout(moveDownSecondPlayer, 4900);
            setTimeout(moveDownSecondPlayer, 5000);
        }
    };

    $scope.shoot = function () {

        if ($scope.isShellChosen === false) {
            $scope.chooseShell = "Choose a shell to fire!";
            $scope.showChooseShell = true;
        }
        else {
            $scope.showChooseShell = false;
            if($scope.isRocketChosen){
                $scope.reduceRocket();
            }
            if($scope.isBombChosen){
                $scope.reduceBomb();
            }
            if($scope.isNuclearChosen){
                $scope.reduceNuclearBomb();
            }
            moveLocalPlayer(movementTypes.shoot);
            $scope.turnEnd();

            if ($scope.inPractice) {
                $scope.compTurns();
                $scope.isUserTurn = false;
                $scope.$apply ();
            }
        }
    };

    function moveRightFirstPlayer() {

        trackPaddingXPlayer = trackPaddingXPlayer + moveSpacing;
        frontRollerPaddingXPlayer = frontRollerPaddingXPlayer + moveSpacing;
        rearRollerPaddingXPlayer = rearRollerPaddingXPlayer + moveSpacing;
        towerPaddingXPlayer = towerPaddingXPlayer + moveSpacing;
        towerPaddingXPlayerR = towerPaddingXPlayerR + moveSpacing;
        gunRotationPointXPlayer = gunRotationPointXPlayer + moveSpacing;

        if (trackPaddingXPlayer > maxTrackPaddingXPlayer) {

            trackPaddingXPlayer = maxTrackPaddingXPlayer;
            frontRollerPaddingXPlayer = maxFrontRollerPaddingXPlayer;
            rearRollerPaddingXPlayer = maxRearRollerPaddingXPlayer;
            towerPaddingXPlayer = maxTowerPaddingXPlayer;
            towerPaddingXPlayerR = maxTowerPaddingXPlayerR;
            gunRotationPointXPlayer = maxGunRotationPointXPlayer;
        }

        $scope.clearMap ();
        $scope.drawField ();
        $scope.drawTanks ();

    }

    function moveLeftFirstPlayer() {

        trackPaddingXPlayer = trackPaddingXPlayer - moveSpacing;
        frontRollerPaddingXPlayer = frontRollerPaddingXPlayer - moveSpacing;
        rearRollerPaddingXPlayer = rearRollerPaddingXPlayer - moveSpacing;
        towerPaddingXPlayer = towerPaddingXPlayer - moveSpacing;
        towerPaddingXPlayerR = towerPaddingXPlayerR - moveSpacing;
        gunRotationPointXPlayer = gunRotationPointXPlayer - moveSpacing;

        if (trackPaddingXPlayer < minTrackPaddingXPlayer) {

            trackPaddingXPlayer = minTrackPaddingXPlayer;
            frontRollerPaddingXPlayer = minFrontRollerPaddingXPlayer;
            rearRollerPaddingXPlayer = minRearRollerPaddingXPlayer;
            towerPaddingXPlayer = minTowerPaddingXPlayer;
            towerPaddingXPlayerR = minTowerPaddingXPlayerR;
            gunRotationPointXPlayer = minGunRotationPointXPlayer;
        }

        $scope.clearMap ();
        $scope.drawField ();
        $scope.drawTanks ();
    }

    function moveRightSecondPlayer() {

        trackPaddingXEnemy = trackPaddingXEnemy + moveSpacing;
        frontRollerPaddingXEnemy = frontRollerPaddingXEnemy + moveSpacing;
        rearRollerPaddingXEnemy = rearRollerPaddingXEnemy + moveSpacing;
        towerPaddingXEnemy = towerPaddingXEnemy + moveSpacing;
        towerPaddingXEnemyR = towerPaddingXEnemyR + moveSpacing;
        gunRotationPointXEnemy = gunRotationPointXEnemy + moveSpacing;

        if (trackPaddingXEnemy > maxTrackPaddingXEnemy) {

            trackPaddingXEnemy = maxTrackPaddingXEnemy;
            frontRollerPaddingXEnemy = maxRearRollerPaddingXEnemy;
            rearRollerPaddingXEnemy = maxFrontRollerPaddingXEnemy;
            towerPaddingXEnemy = maxTowerPaddingXEnemy;
            towerPaddingXEnemyR = maxTowerPaddingXEnemyR;
            gunRotationPointXEnemy = maxGunRotationPointXEnemy;
        }

        $scope.clearMap ();
        $scope.drawField ();
        $scope.drawTanks ();

    }

    function moveLeftSecondPlayer() {

        trackPaddingXEnemy = trackPaddingXEnemy - moveSpacing;
        frontRollerPaddingXEnemy = frontRollerPaddingXEnemy - moveSpacing;
        rearRollerPaddingXEnemy = rearRollerPaddingXEnemy - moveSpacing;
        towerPaddingXEnemy = towerPaddingXEnemy - moveSpacing;
        towerPaddingXEnemyR = towerPaddingXEnemyR - moveSpacing;
        gunRotationPointXEnemy = gunRotationPointXEnemy - moveSpacing;

        if (trackPaddingXEnemy < minTrackPaddingXEnemy) {

            trackPaddingXEnemy = minTrackPaddingXEnemy;
            frontRollerPaddingXEnemy = minFrontRollerPaddingXEnemy;
            rearRollerPaddingXEnemy = minRearRollerPaddingXEnemy;
            towerPaddingXEnemy = minTowerPaddingXEnemy;
            towerPaddingXEnemyR = minTowerPaddingXEnemyR;
            gunRotationPointXEnemy = minGunRotationPointXEnemy;
        }

        $scope.clearMap ();
        $scope.drawField ();
        $scope.drawTanks ();
    }

    function moveUpFirstPlayer() {

        gunAnglePlayer = gunAnglePlayer + rotationAngleSpacing;

        if (gunAnglePlayer > maxRotationAnglePlayer) {

            gunAnglePlayer = maxRotationAnglePlayer;
        }

        $scope.clearMap ();
        $scope.drawField ();
        $scope.drawTanks ();
    }

    function moveDownFirstPlayer() {

        gunAnglePlayer = gunAnglePlayer - rotationAngleSpacing;

        if (gunAnglePlayer < minRotationAnglePlayer) {

            gunAnglePlayer = minRotationAnglePlayer;
        }

        $scope.clearMap ();
        $scope.drawField ();
        $scope.drawTanks ();
    }

    function moveUpSecondPlayer() {

        gunAngleEnemy = gunAngleEnemy + rotationAngleSpacing;

        if (gunAngleEnemy > minRotationAngleEnemy) {

            gunAngleEnemy = minRotationAngleEnemy;
        }

        $scope.clearMap ();
        $scope.drawField ();
        $scope.drawTanks ();
    }

    function moveDownSecondPlayer() {

        gunAngleEnemy = gunAngleEnemy - rotationAngleSpacing;

        if (gunAngleEnemy < maxRotationAngleEnemy) {

            gunAngleEnemy = maxRotationAngleEnemy;
        }

        $scope.clearMap ();
        $scope.drawField ();
        $scope.drawTanks ();
    }
}]);