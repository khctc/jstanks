var app = angular.module('UoT', []).controller('mainController', ['$scope', function ($scope) {

    $scope.showMainPage = false;
    $scope.showUserNameForm = false;
    $scope.showMainMenu = false;
    $scope.showRoomCreatePage = false;
    $scope.showRoomPage = false;
    $scope.showLobbyPage = false;
    $scope.showBattlePage = false;
    $scope.showConfirmationPage = false;
    $scope.showResultPage = false;
    $scope.showShopPage = false;
    $scope.showResultPage = false;
    $scope.showTimerPage= false;

    $scope.error = false;
    $scope.errorUserRegistered = false;
    $scope.errorSocketConnection = false;

    $scope.inPractice = false;

    $scope.UserName = "Player";

    $scope.verifyUserName = function (username) {
      $scope.$broadcast(events.VerifyUserNameEvent,username);
    };

    $scope.$on(events.VerifyUserNameSuccessEvent, function (event, allowUser) {

        $scope.$apply(function () {

            if (allowUser === true) {
                $scope.goToMenu();
            }
            else {
                $scope.error = true;
                $scope.errorUserRegistered = true;
            }
        });
    });

    $scope.$on(events.ErrorConnectToServerEvent,function (event, arg) {
        $scope.error = true;
        $scope.errorSocketConnection = true;
    });

    $scope.enableMainPage = function() {
        $scope.showMainPage = true;
        $scope.showUserNameForm = true;
        $scope.showTimerPage= false;
    };

    $scope.disableMainPage = function() {
        $scope.showMainPage = false;
        $scope.showUserNameForm = false;
    };

    $scope.enableMainMenu = function() {
        $scope.showMainMenu = true;
        $scope.showTimerPage= false;
    };

    $scope.disableMainMenu = function() {
        $scope.showMainMenu = false;
    };

    $scope.enableRoomCreate = function() {
        $scope.showRoomCreatePage = true;
        $scope.showTimerPage= false;
    };

    $scope.disableRoomCreate = function() {
        $scope.showRoomCreatePage = false;
    };

    $scope.enableRoomPage = function() {
        $scope.showRoomPage = true;
        $scope.showTimerPage= false;
    };

    $scope.disableRoomPage = function() {
        $scope.showRoomPage = false;
    };

    $scope.enableLobby = function() {
        $scope.showLobbyPage = true;
        $scope.showTimerPage= false;
    };

    $scope.disableLobby = function() {
        $scope.showLobbyPage = false;
    };

    $scope.enableBattle = function() {
        $scope.showBattlePage = true;
        $scope.showTimerPage= false;
    };

    $scope.disableBattle = function() {
        $scope.showBattlePage = false;
    };

    $scope.enableConfirmation = function() {
        $scope.showConfirmationPage = true;
        $scope.showTimerPage= false;
    };

    $scope.disableConfirmation = function() {
        $scope.showConfirmationPage = false;
    };

    $scope.enableResult = function() {
        $scope.showResultPage = true;
        $scope.showTimerPage= false;
    };

    $scope.disableResult = function() {
        $scope.showResultPage = false;
    };

    $scope.enableShop = function () {
        $scope.showShopPage = true;
    };

    $scope.disableShop = function () {
        $scope.showShopPage = false;
    };

    $scope.enableTimer = function () {
        $scope.showTimerPage = true;
    };

    $scope.disableTimer = function () {
        $scope.showTimerPage = false;
    };

    window.onload = function() {
        $scope.enableMainPage();
    };

    $scope.goToMenu = function() {

        if ($scope.showUserNameForm) {

            $scope.$broadcast(events.UsernameUpdateEvent, $scope.UserName);
            $scope.$broadcast(events.AddConnectionEvent, $scope.UserName);
        }
        $scope.$broadcast(events.GoToMenuEvent, null);
    };

    $scope.goToMenu = function() {

        if ($scope.showUserNameForm) {

            $scope.$broadcast(events.UsernameUpdateEvent, $scope.UserName);
            $scope.$broadcast(events.AddConnectionEvent, $scope.UserName);
        }
        $scope.$broadcast(events.GoToMenuEvent, null);
    };

    $scope.goToMainPage = function () {
        $scope.disableMainMenu();
        $scope.enableMainPage();
        $scope.$broadcast(events.RemoveConnectionEvent, $scope.UserName);
    };

}]);