angular.module('UoT').controller('WebSocketController', ['$scope', function ($scope)
{

    var callbacks = {};
    var currentCallbackId = 0;

    var ws = new WebSocket("wss://jstanks.herokuapp.com");

    ws.onerror = function () {
        $scope.$emit(events.ErrorConnectToServerEvent, null);
    };

    ws.onclose = function () {
        $scope.$emit(events.ErrorConnectToServerEvent, null);
    };

    ws.onopen = function () {

    };

    ws.onmessage = function (message) {

        try {
            listener(JSON.parse(message.data));
        }
        catch (Exception) {

        }
    };

    function sendRequest(request) {
        var callbackId = getCallbackId();
        callbacks[ callbackId ] = {
            time: new Date()
        };
        request.callback_id = callbackId;
        ws.send(JSON.stringify(request));
    }

    function listener(data) {

        var messageObj = data;
        // If an object exists with callback_id in our callbacks object, resolve it
        if (callbacks.hasOwnProperty(messageObj.callback_id) || messageObj.callback_id == "force") {

            var playersData;

            if (messageObj.requestType === requestTypes.getRooms) {
                $scope.$broadcast(events.RoomsListUpdateEvent, messageObj.roomsList);
            }
            else if (messageObj.requestType === requestTypes.addToRoom) {
                playersData = {
                    players: messageObj.players,
                    playerData: messageObj.playerData
                };
                $scope.$broadcast(events.AddToRoomSuccessEvent, playersData);
            }
            else if (messageObj.requestType === requestTypes.leaveFromRoom) {
                playersData = {
                    players: messageObj.players,
                    playerData: messageObj.playerData
                };
                $scope.$broadcast(events.LeaveFromRoomSuccessEvent, playersData);
            }
            else if (messageObj.requestType === requestTypes.verifyUser) {
                $scope.$emit(events.VerifyUserNameSuccessEvent, messageObj.allowUser);
            }
            else if (messageObj.requestType === requestTypes.updatePlayerData) {

                $scope.$broadcast(events.UpdatePlayerDataSuccessEvent, messageObj.playerData);
            }
            else if (messageObj.requestType === requestTypes.startGame) {

                $scope.$broadcast(events.startGameEventSuccess, data.game);
            }
            else if (messageObj.requestType === requestTypes.readyPlayer) {

                $scope.$broadcast(events.NewReadyPlayerEvent, messageObj.player);
            }
            else if (messageObj.requestType === requestTypes.playerMove) {

                var moveObj = {
                    player: messageObj.player,
                    movementType: messageObj.movementType
                };

                $scope.$broadcast(events.PlayerMoveUpdateEvent, moveObj);
            }
            else if (messageObj.requestType === requestTypes.changeTurn) {

                $scope.$broadcast(events.ChangeTurnSuccessEvent, null);
            }
            else if (messageObj.requestType === requestTypes.changeWeapon) {

                $scope.$broadcast(events.ChangeWeaponSuccessEvent, messageObj.weapon);
            }
            try {
                if (data.callback_id !== "force")
                    delete callbacks[ data.callback_id ];
            }
            catch (ignored) {

            }
        }
    }

    function getCallbackId() {
        currentCallbackId += 1;
        if (currentCallbackId > 10000) {
            currentCallbackId = 0;
        }
        return currentCallbackId;
    }

    $scope.$on(events.AddLobbyListenerEvent, function (event, player) {

        var request = {
            requestType: requestTypes.getRooms,
            player: player
        };
        sendRequest(request);
    });


    $scope.$on(events.CreateRoomEvent, function (event, roomData) {

        var request = {
            requestType: requestTypes.createRoom,
            roomData: roomData
        };
        sendRequest(request);
    });


    $scope.$on(events.VerifyUserNameEvent, function (event, username) {

        var request = {
            requestType: requestTypes.verifyUser,
            player: username
        };
        sendRequest(request);
    });

    $scope.$on(events.LeaveFromRoomEvent, function (event, args) {

        var request = {
            requestType: requestTypes.leaveFromRoom,
            owner: args.owner,
            player: args.player
        };
        sendRequest(request);
    });

    $scope.$on(events.AddToRoomEvent, function (event, args) {

        var request = {
            requestType: requestTypes.addToRoom,
            owner: args.owner,
            player: args.player
        };
        sendRequest(request);
    });

    $scope.$on(events.AddConnectionEvent, function (event, player) {

        var request = {
            requestType: requestTypes.addConnection,
            ConnectionOwner: player
        };
        sendRequest(request);
    });

    $scope.$on(events.RemoveConnectionEvent, function (event, player) {

        var request = {
            requestType: requestTypes.removeConnection,
            ConnectionOwner: player
        };
        sendRequest(request);
    });


    $scope.$on(events.UpdatePlayerDataEvent, function (event, transportObj) {

        var request = {
            requestType: requestTypes.updatePlayerData,
            owner: transportObj.owner,
            playerData: transportObj.data
        };
        sendRequest(request);
    });

    $scope.$on(events.startGameEvent, function (event, room) {
        var request = {
            requestType: requestTypes.startGame,
            owner: room.owner,
            hits: room.hits,
            wind: room.wind,
            rounds: room.rounds,
            money: room.money,
            playerData: room.playerData
        };
        sendRequest(request);
    });

    $scope.$on(events.PlayerMoveEvent, function (event, args) {
        var move = args.movementType;
        var game = args.game;

        var request = {
            requestType: requestTypes.playerMove,
            game: game,
            player: $scope.UserName,
            movementType: move
        };
        sendRequest(request);
    });

    $scope.$on(events.ReadyEvent, function (event, player) {
        var request = {
            requestType: requestTypes.readyPlayer,
            player: player
        };
        sendRequest(request);
    });

    $scope.$on(events.ChangeTurnEvent, function(event, game) {
        var request = {
            requestType: requestTypes.changeTurn,
            game : game

        };
        sendRequest(request);
    });
    $scope.$on(events.ChangeWeaponEvent, function(event, obj) {
        var request = {
            requestType: requestTypes.changeWeapon,
            game : obj.game,
            weapon : obj.weapon

        };
        sendRequest(request);
    });
}]);