angular.module('UoT').controller('LobbyController', [
    '$scope',
    function ($scope)
    {

        $scope.roomsList = [];

        $scope.UserName = "";
        $scope.colorsList = colors;

        $scope.gameStartPermission = false;
        $scope.gameNotFull = true;

        /**
         * data of one game (room)
         * owner - room creator
         * roomName - name of room for lobby
         * playerData - set of players, that contains all players data
         *  - player
         *  - color
         *  - weapon
         * @type {{owner: string, roomName: string, money: string, wind: string, hits: string, rounds: string, players:
     *     Array, colors: {}}}
         */
        $scope.roomData = {
            owner: "",
            roomName: "Default",
            money: 100,
            wind: "None",
            hits: 100,
            rounds: 1,
            players: [],
            playerData: []
        };

        //Local functions

        function findLocalPlayer(element, index, args)
        {

            return $scope.isCurrentUser(element.player);
        }

        function removeUserFromRoom()
        {

            var playerData = $scope.roomData.playerData.find(findLocalPlayer);
            $scope.roomData.playerData.splice($scope.roomData.playerData.indexOf(playerData), 1);
        }


        $scope.createRoom = function ()
        {

            $scope.gameStartPermission = true;

            $scope.$emit(events.CreateRoomEvent, $scope.roomData);
            $scope.$emit(events.GoToRoomEvent, null);
        };

        $scope.addToRoom = function (id)
        {

            function findRoom(element, index, array)
            {

                if (element.owner === id) {
                    return true;
                }
            }

            $scope.roomData = $scope.roomsList.find(findRoom);
            $scope.$emit(events.GoToRoomEvent, null);

            var args = {
                owner  : id,
                player: $scope.UserName
        };
            $scope.$emit(events.AddToRoomEvent, args);
        };

        $scope.$on(events.AddToRoomSuccessEvent, function (event, obj)
        {

            $scope.$apply(function ()
            {

                $scope.gameNotFull = false;

                $scope.roomData.players = obj.players;
                $scope.roomData.playerData = obj.playerData;
            });
        });

        $scope.leaveFromRoom = function ()
        {

            removeUserFromRoom();

            var args = {
                owner: $scope.roomData.owner,
                player: $scope.UserName
            };
            $scope.$emit(events.LeaveFromRoomEvent, args);

            if ($scope.UserName === $scope.roomData.owner) {
                $scope.$emit(events.GoToMenuEvent, null);
            }
            else {
                $scope.$emit(events.GoToLobbyEvent, null);
            }
        };
        $scope.$on(events.LeaveFromRoomSuccessEvent, function (event, data)
        {

            $scope.$apply(function ()
            {

                $scope.roomData.players = data.players;
                $scope.roomData.playerData = data.playerData;
            });

            //If owner has went out from room

            if ($scope.roomData.players.indexOf($scope.roomData.owner) === -1) {

                $scope.$emit(events.GoToLobbyEvent, null);
            }
        });

        $scope.startGame = function ()
        {

            // Auto change color for players, who not changed it
            var tmpColors = $scope.colorsList;
            var playerWithoutColors = [];
            for (var i = 0; i < $scope.roomData.playerData.length; i++) {

                if ($scope.roomData.playerData[ i ].color === "") {

                    playerWithoutColors.push(i);
                }
                else {
                    tmpColors.splice(tmpColors.indexOf($scope.roomData.playerData[ i ].color), 1);
                }
            }
            for (i = 0; i < playerWithoutColors.length; i++) {

                $scope.roomData.playerData[ playerWithoutColors[ i ] ].color = tmpColors[ i ];
            }

            $scope.$emit(events.startGameEvent, $scope.roomData);
        };

        $scope.$on(events.startGameEventSuccess, function (event, arg)
        {

            $scope.$apply(function ()
            {

                $scope.goToShop($scope.roomData.money);
            });
        });

        $scope.getRoomsList = function ()
        {
            $scope.$emit(events.AddLobbyListenerEvent, null);
        };

        $scope.$on(events.RoomsListUpdateEvent, function (event, roomList)
        {

            $scope.$apply(function ()
            {
                $scope.roomsList = roomList;
            });
        });

        $scope.$on(events.UsernameUpdateEvent, function (event, username)
        {

            if ($scope.roomData.players.lastIndexOf(username, 0) === -1) {
                $scope.UserName = username;
                $scope.roomData.owner = username;
                $scope.roomData.players.push(username);

                var playerData = {
                    player: username,
                    color: ""
                };
                $scope.roomData.playerData.push(playerData);
            }
        });

        $scope.returnToMenu = function ()
        {
            $scope.$emit(events.GoToMenuEvent, null);
        };

        $scope.isCurrentUser = function (player)
        {
            return player === $scope.UserName;
        };

        $scope.roomFilter = function (value, index, www)
        {
            if (value.players.length < 2) {
                return true;
            }
        };

        //Filter for colors list in room view

        $scope.colorFilter = function (value, index, www)
        {
            for (var i = 0; i < $scope.roomData.playerData.length; i++) {
                if (value === $scope.roomData.playerData[ i ].color) {
                    return false;
                }
            }
            return true;
        };

        //Update color when it has been changed in room

        $scope.changeColor = function (color)
        {

            $scope.roomData.playerData.find(findLocalPlayer).color = color;

            var transportObj = {
                owner: $scope.roomData.owner,
                data: $scope.roomData.playerData
            };

            $scope.$emit(events.UpdatePlayerDataEvent, transportObj);
        };

        $scope.$on(events.UpdatePlayerDataSuccessEvent, function (event, playerData)
        {

            $scope.$apply(function ()
            {
                $scope.roomData.playerData = playerData;
            });
        });
    }
]);