angular.module('UoT').controller('menuController', ['$scope', function ($scope)
{

    $scope.goToPractice = function () {
        $scope.disableMainMenu();
        $scope.enableBattle();
        $scope.inPractice = true;

        var transportObj = {
            weapon : $scope.weapon,
            money : 100
        };

        $scope.game = undefined;

        // $scope.createGameForPractice();

        // $scope.$broadcast(events.ShowBattleEvent, transportObj);
        $scope.$broadcast(events.DrawCanvasEvent, null);
    };

    $scope.createGameForPractice = function createGameForPractice () {
        $scope.game = new clientGame("None", 100, 1);

        /*for (var i = 0; i < game.playerData.length; i++) {
            $scope.game.addPlayerData(game.playerData[i].player, game.playerData[i].color, game.playerData[i].money);
        }*/
        $scope.ownerHP = $scope.game.hits;
        $scope.enemyHP = $scope.game.hits;
    };
    
    $scope.$on(events.GoToBattleEvent, function (event, transportObj) {
        $scope.goToBattle(transportObj);
    });

    $scope.goToBattle = function (transportObj) {
        
        $scope.$apply(function () {
            
            $scope.disableMainMenu();
            $scope.disableShop();
            $scope.disableResult();
            $scope.enableBattle();
        });

        $scope.$broadcast(events.ShowBattleEvent, transportObj);
        $scope.$broadcast(events.DrawCanvasEvent,null);
    };

    $scope.$on(events.GoToMenuEvent, function (event, args) {
        $scope.enableMainMenu();
        $scope.disableMainPage();
        $scope.disableRoomPage();
        $scope.disableRoomCreate();
        $scope.disableLobby();
    });

    $scope.goToRoomCreatePage = function () {
        $scope.disableMainMenu();
        $scope.enableRoomCreate();
    };

    $scope.$on(events.GoToRoomEvent, function (event, args) {
        $scope.goToRoomPage();
    });

    $scope.goToRoomPage = function () {
        $scope.disableRoomCreate();
        $scope.enableRoomPage();
        $scope.disableLobby();
    };

    $scope.$on(events.GoToLobbyEvent, function (event, args) {
        $scope.disableRoomPage();
        $scope.goToLobbyPage();
    });

    $scope.goToLobbyPage = function () {
        $scope.$broadcast(events.AddLobbyListenerEvent, $scope.UserName);
        $scope.disableMainMenu();
        $scope.disableShop();
        $scope.disableTimer();
        $scope.enableLobby();
    };

    $scope.goToShop = function (money) {
        $scope.disableRoomPage();
        $scope.disableLobby();
        $scope.enableShop();
        $scope.enableTimer();

        $scope.$broadcast(events.GoToShopEvent, money);
        $scope.$broadcast(events.CloseResultEvent, null);
    };
}]);