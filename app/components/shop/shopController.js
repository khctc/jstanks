angular.module('UoT').controller('shopController', ['$scope', function ($scope)
{
    // View money
    $scope.money = 0;
    $scope.countDown = 0;

    // Local money
    var money = 0;

    var shopTimerObject = null;

    $scope.isPlayerReady = false;

    $scope.weapon = {
        rocket: 0,
        bomb: 0,
        nuclearBomb: 0,
        piersingRadius: 3,
        rocketRadius: 5,
        bombRadius: 7,
        nuclearBombRadius: 9,
        piersingDamage: 5,
        rocketDamage: 10,
        bombDamage: 15,
        nuclearBombDamage: 20
    };

    $scope.rocketPrice = 7;
    $scope.bombPrice = 9;
    $scope.nuclearBombPrice = 15;

    $scope.$on(events.GoToShopEvent, function (event, money) {
        $scope.money = money;
        $scope.shopCountDown = 30;

        if (shopTimerObject === null) {
            shopTimerObject = setInterval(function () {

                if ($scope.shopCountDown > 0) {
                    $scope.shopCountDown--;
                    $scope.$apply();
                }
                if ($scope.shopCountDown === 0) {
                    $scope.isPlayerReady = true;
                    $scope.$emit(events.ReadyEvent, $scope.UserName);
                }
            }, 1000);
        }
    });

    //Update money
    $scope.ready = function () {
        money = $scope.money - ($scope.weapon.rocket * $scope.rocketPrice) - ($scope.weapon.bomb * $scope.bombPrice) - ($scope.weapon.nuclearBomb * $scope.nuclearBombPrice);
        $scope.isPlayerReady = true;
        $scope.$emit(events.ReadyEvent,$scope.UserName);
    };

    $scope.$on(events.AllPlayersReadyEvent, function (event,arg) {
        var transportObj = {
            weapon : $scope.weapon,
            money : money
        };

        clearInterval(shopTimerObject);

        $scope.$emit(events.GoToBattleEvent,transportObj);
    });

    $scope.reset = function () {

        $scope.weapon = angular.copy({
            rocket: 0,
            bomb: 0,
            nuclearBomb: 0
        });
    }
}]);
