/**
 * Init a game
 * @param wind
 * @param hits
 * @param rounds
 * @constructor
 */
function clientGame(wind, hits, rounds) {
    this.wind = wind;
    this.hits = hits;
    this.rounds = rounds;
    this.roundsPast = 0;
    this.playerData = [];
    this.walkingPlayer = undefined;
}
/**
 * return playerData if exist, or false if undefined
 * @param player
 */
clientGame.prototype.getPlayerData = function (player) {
    var playerData = this.searchPlayerData(player);
    if (playerData === undefined) {

    }
    else {
        return playerData;
    }
};
/**
 * added playerData to game
 * @param player
 * @param color
 * @param money
 */
clientGame.prototype.addPlayerData = function (player, color, money) {
    if (player !== undefined && color !== undefined && money !== undefined) {
        var playerData = {
            player: player,
            color: color,
            money: money,
            ready: false
        };

        this.playerData.push(playerData);
    }
    else {

    }
};
/**
 * remove playerData from game
 * @param player
 */
clientGame.prototype.removePlayerData = function (player) {
    var data = this.searchPlayerData(player);
    this.playerData.splice(this.playerData.indexOf(data), 1);
};
/**
 * set money for player
 * @param player
 * @param money
 */
clientGame.prototype.setMoney = function (player, money) {
    var data = this.searchPlayerData(player);
    data.money = money;
};
/**
 * return money of player
 * @param player
 * @param money
 * @returns {*}
 */
clientGame.prototype.getMoney = function (player, money) {
    var data = this.searchPlayerData(player);
    return data.money;
};
/**
 * Increment to next round if possible and return true.
 * If all rounds have been played return false
 * @returns {boolean}
 */
clientGame.prototype.nextRound = function () {
    if (this.roundsPast <= this.rounds) {
        this.roundsPast++;
        return true;
    }
    else {
        return false;
    }
};
/**
 * return players array
 * @returns {Array}
 */
clientGame.prototype.getPlayers = function () {
    var players = [];

    for (var i = 0; i < this.playerData.length; i++) {
        players.push(this.playerData[ i ].player);
    }
    return players;
};
/**
 * set ready state for player
 * @param player
 * @param {boolean} value
 */
clientGame.prototype.setReady = function (player, value) {
    try {
        this.getPlayerData(player).ready = value;
    }
    catch (e) {

    }
};
/**
 * change stroke to new player in players set
 */
clientGame.prototype.changeTurn = function () {
    var players = this.getPlayers();

    // make first turn for server
    if (this.walkingPlayer === undefined) {
        this.walkingPlayer = players[0];
        return;
    }
    for (var i = 0; i < players.length; i++) {
        // if all players turned
        if (i === players.length - 1) {
            this.walkingPlayer = players[0];
            break;
        }
        // turn of next player in set
        else if (this.walkingPlayer === players[i]) {
            this.walkingPlayer = players[i+1];
            break;
        }
    }
};

clientGame.prototype.getWalkingPlayer = function () {
    return this.walkingPlayer;
};
clientGame.prototype.searchPlayerData = function (player) {

    function search(el, index, args) {
        if (el.player === player)
            return true;
    }

    var data = this.playerData.find(search);
    if (data === undefined) {
        return;
    }
    return data;
};

