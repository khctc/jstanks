var requestTypes = {
    addConnection: "addConnection",
    removeConnection: "removeConnection",
    createRoom: "createRoom",
    getRooms: "getRooms",
    addToRoom: "addToRoom",
    leaveFromRoom: "leaveFromRoom",
    startGame: "startGame",
    verifyUser: "verifyUser",
    readyPlayer: "readyPlayer",
    updatePlayerData : "updatePlayerData",
    playerMove : "playerMove",
    changeTurn : "changeTurn",
    changeWeapon : "changeWeapon",
    playerQuit: "playerQuit"
};

var weaponTypes =
{
    piersing : "piersing",
    rocket : "rocket",
    bomb : "bomb",
    nuclearBomb : "nuclearBomb"
};

var movementTypes =
{
    moveLeft : "moveLeft",
    moveRight : "moveRight",
    moveUp : "moveUp",
    moveDown : "moveDown",
    shoot: "shoot"
};

var colors = [
    '#0000AA','#00AA00','#00AAAA','#AA0000','#AA00AA','#FFAA00','#AAAAAA','#555555','#5555FF','#55FF55','#55FFFF','#FF5555','#FF55FF','#FFFF55'
];

var events = {
    // battle events
    startGameEvent : "startGameEvent",
    startGameEventSuccess: "startGameEventSuccess",
    ShowBattleEvent: 'ShowBattleEvent',
    ReadyEvent: 'ReadyEvent',
    NewReadyPlayerEvent : 'NewReadyPlayerEvent',
    AllPlayersReadyEvent : 'AllPlayersReadyEvent',
    DrawCanvasEvent : 'DrawCanvasEvent',
    CloseResultEvent: 'CloseResultEvent',
    PlayerMoveEvent : "PlayerMoveEvent",
    PlayerShootEvent : "PlayerShootEvent",
    PlayerMoveUpdateEvent : "PlayerMoveUpdateEvent",
    ChangeTurnEvent : "ChangeTurnEvent",
    ChangeTurnSuccessEvent : "ChangeTurnSuccessEvent",
    ChangeWeaponEvent : "ChangeWeaponEvent",
    ChangeWeaponSuccessEvent : "ChangeWeaponSuccessEvent",
    OwnerWinRoundEvent : "OwnerWinRoundEvent",
    EnemyWinRoundEvent : "EnemyWinRoundEvent",
    PlayerQuitEvent : "PlayerQuitEvent",

    // screens events
    GoToMenuEvent: 'GoToMenuEvent',
    GoToRoomEvent: 'GoToRoomEvent',
    GoToLobbyEvent: 'GoToLobbyEvent',
    GoToShopEvent: 'GoToShopEvent',
    GoToBattleEvent: 'GoToBattleEvent',

    // connection control events
    AddConnectionEvent: 'AddConnectionEvent',
    RemoveConnectionEvent: 'RemoveConnectionEvent',

    // room events
    CreateRoomEvent: 'CreateRoomEvent',
    AddToRoomEvent: 'AddToRoomEvent',
    AddToRoomSuccessEvent: 'AddToRoomSuccessEvent',
    AddLobbyListenerEvent: 'AddLobbyListenerEvent',
    RoomsListUpdateEvent: 'RoomsListUpdateEvent',
    UpdatePlayerDataEvent: 'UpdatePlayerDataEvent',
    UpdatePlayerDataSuccessEvent: 'UpdatePlayerDataSuccessEvent',
    LeaveFromRoomEvent: 'LeaveFromRoomEvent',
    LeaveFromRoomSuccessEvent: 'LeaveFromRoomSuccessEvent',


    // local player events
    VerifyUserNameEvent: 'VerifyUserNameEvent',
    VerifyUserNameSuccessEvent: 'VerifyUserNameSuccessEvent',
    ErrorConnectToServerEvent: 'ErrorConnectToServerEvent',
    UsernameUpdateEvent: 'UsernameUpdateEvent'
};