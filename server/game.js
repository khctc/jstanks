/**
 * Init a game
 * @param wind
 * @param hits
 * @param rounds
 * @constructor
 */
function Game(wind, hits, rounds) {
    this.wind = wind;
    this.hits = hits;
    this.rounds = rounds;
    this.roundsPast = 0;
    this.playerData = [];
}
/**
 * return playerData if exist, or false if undefined
 * @param player
 */
Game.prototype.getPlayerData = function (player) {
    var playerData = this.searchPlayerData(player);
    if (playerData === undefined) {

    }
    else {
        return playerData;
    }
};
/**
 * added playerData to game
 * @param player
 * @param color
 * @param money
 */
Game.prototype.addPlayerData = function (player, color, money) {
    if (player !== undefined && color !== undefined && money !== undefined) {
        var playerData = {
            player: player,
            color: color,
            money: money,
            ready: false
        };

        this.playerData.push(playerData);
    }
    else {

    }
};
/**
 * remove playerData from game
 * @param player
 */
Game.prototype.removePlayerData = function (player) {
    var data = this.searchPlayerData(player);
    this.playerData.splice(this.playerData.indexOf(data), 1);
};
/**
 * set money for player
 * @param player
 * @param money
 */
Game.prototype.setMoney = function (player, money) {
    var data = searchPlayerData(player);
    data.money = money;
};
/**
 * return money of player
 * @param player
 * @param money
 * @returns {*}
 */
Game.prototype.getMoney = function (player, money) {
    var data = searchPlayerData(player);
    return data.money;
};
/**
 * Increment to next round if possible and return true.
 * If all rounds have been played return false
 * @returns {boolean}
 */
Game.prototype.nextRound = function () {
    if (this.roundsPast <= this.rounds) {
        this.roundsPast++;
        return true;
    }
    else {
        return false;
    }
};
/**
 * return players array
 * @returns {Array}
 */
Game.prototype.getPlayers = function () {
    var players = [];

    for (var i = 0; i < this.playerData.length; i++) {
        players.push(this.playerData[ i ].player);
    }
    return players;
};
/**
 * set ready state for player
 * @param player
 * @param {boolean} value
 */

Game.prototype.setReady = function (player, value) {
    try {
        this.getPlayerData(player).ready = value;
    }
    catch (e) {

    }
};


Game.prototype.searchPlayerData = function (player) {
    function search(el, index, args) {
        if (el.player === player)
            return true;
    }

    var data = this.playerData.find(search);
    if (data === undefined) {
        return;
    }

    return data;
};

exports.Game = Game;

