function Room(data) {
    this.owner = data.owner;
    this.roomName = data.roomName;
    this.money = data.money;
    this.wind = data.wind;
    this.hits = data.hits;
    this.rounds = data.rounds;
    this.players = [data.owner];
    this.playerData = [
        {
            player: data.owner,
            color: ""
        }
    ];
}

exports.Room = Room;