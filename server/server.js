var room = require('./room');
var game = require('./game');

var requestTypes = {
    addConnection: "addConnection",
    removeConnection: "removeConnection",
    createRoom: "createRoom",
    getRooms: "getRooms",
    addToRoom: "addToRoom",
    leaveFromRoom: "leaveFromRoom",
    startGame: "startGame",
    verifyUser: "verifyUser",
    readyPlayer: "readyPlayer",
    updatePlayerData : "updatePlayerData",
    playerMove : "playerMove",
    changeTurn : "changeTurn",
    changeWeapon : "changeWeapon",
    playerQuit: "playerQuit"
};

var connections = [];
var lobbyPlayers = [];
var gamesList = [];
var roomsList = [];


var WebSocketServer = require('ws').Server, wss = new WebSocketServer({port: 2228});

wss.on('connection', function (ws) {
    ws.on('message', function (req) {
        try {
            var data = JSON.parse(req);

            switch (data.requestType) {
                case requestTypes.addConnection:
                    addConnection(data.ConnectionOwner, data.callback_id, ws);
                    break;
                case requestTypes.removeConnection:
                    removeConnection(data.ConnectionOwner, data.callback_id, ws);
                    break;
                case requestTypes.createRoom:
                    createRoom(data.roomData, data.callback_id, ws);
                    break;
                case requestTypes.getRooms:
                    getRooms(data.player, data.callback_id, ws);
                    break;
                case requestTypes.addToRoom:
                    addToRoom(data.owner, data.player);
                    break;
                case requestTypes.leaveFromRoom:
                    leaveFromRoom(data.owner, data.player, data.callback_id);
                    break;
                case requestTypes.verifyUser:
                    verifyUserName(data.player, data.callback_id, ws);
                    break;
                case requestTypes.updatePlayerData:
                    updatePlayerData(data.owner, data.playerData, data.callback_id, ws);
                    break;
                case requestTypes.startGame:
                    startGame(data.owner, data.money, data.hits, data.wind, data.rounds, data.playerData, data.callback_id);
                    break;
                case requestTypes.readyPlayer:
                    addReadyPlayer(data.player, data.callback_id);
                    break;
                case requestTypes.playerMove:
                    PlayerMoveUpdate(data.game, data.player, data.movementType, data.callback_id);
                    break;
                case requestTypes.changeTurn:
                    changeTurn(data.game);
                    break;
                case requestTypes.changeWeapon:
                    changeWeapon(data.game,data.weapon);
                    break;
            }
        }
        catch (Exception) {
            console.log("[ERROR] Error while handling data!");
            console.log("[ERROR] Exception:", Exception);
            console.log("[ERROR] Request:", JSON.stringify(data));
        }
    });

    ws.on('close', function () {
        function getArrByWS(element, index, array) {
            if (element.connection === ws)
                return true;
        }

        var connection = connections.find(getArrByWS);

        // wat?? i don't know why connection can be undefined but it can
        if (connection === undefined) {
            return;
        }

        // remove from lobbyPlayers, if exist
        if (lobbyPlayers.indexOf(connection.user) !== -1) {
            function getLobbyObject(element, index, array) {
                if (element == connection.user)
                    return true;
            }

            lobbyPlayers.splice(lobbyPlayers.indexOf(lobbyPlayers.find(getLobbyObject)), 1);
        }

        for (var i = 0; i < roomsList.length; i++) {

            // remove player from room, if he "player" or if this user "owner"
            if (roomsList[ i ].players.indexOf(connection.user) !== -1) {
                leaveFromRoom(roomsList[ i ].owner, connection.user, 0);
            }
        }

        // remove player from game
        for (i = 0; i < gamesList.length; i++) {
            for (var j = 0; j < gamesList[ i ].playerData.length; j++) {
                if (gamesList[ i ].playerData[j].player.indexOf(connection.user) !== -1) {
                    gamesList[ i ].removePlayerData(connection.user);

                    if (gamesList[i ].playerData.length === 0)
                    {
                        gamesList.slice(i,1);
                    }
                    else
                    {
                        // send exit event of other player
                        var request = {
                            callback_id: "force",
                            requestType: requestTypes.playerQuit,
                            winner: gamesList[ i ].playerData[0 ].player
                        };

                        var con = searchConnection(gamesList[ i ].playerData[0 ].player);
                        con.connection.send(JSON.stringify(request));
                    }
                }
            }
        }
        // and remove player from connections
        connections.splice(connections.indexOf(connection), 1);
    });
});


function addConnection(player, callback_id, ws) {
    var array = {
        user: player,
        connection: ws
    };
    connections.push(array);
    var request = {
        callback_id: callback_id,
        text: "Connection successfully"
    };
    ws.send(JSON.stringify(request));
}

function removeConnection(player, callback_id, ws) {
    var userCon = searchConnection(player);

    connections.splice(connections.indexOf(userCon), 1);

    var request = {
        callback_id: callback_id,
        text: "Connection removed successfully"
    };
    ws.send(JSON.stringify(request));
}

function verifyUserName(player, callback_id, ws) {
    var user = searchConnection(player);

    var allowUser = true;
    if (user !== undefined) {
        allowUser = false;
    }

    var request = {
        callback_id: callback_id,
        requestType: requestTypes.verifyUser,
        allowUser: allowUser
    };
    ws.send(JSON.stringify(request));
}

function createRoom(roomData, callback_id, ws) {
    var newRoom = new room.Room(roomData);
    roomsList.push(newRoom);

    // send request to other lobby listeners
    updateRoomsForLobbyPlayers();

    var request = {
        callback_id: callback_id,
        requestType: requestTypes.createRoom,
        text: "Room created successful"
    };
    ws.send(JSON.stringify(request));
}

function getRooms(player, callback_id, ws) {
    if (lobbyPlayers.indexOf(player) === -1)
        lobbyPlayers.push(player);

    var request = {
        callback_id: callback_id,
        requestType: requestTypes.getRooms,
        roomsList: roomsList
    };
    ws.send(JSON.stringify(request));
}

function addToRoom(owner, player) {
    var room = searchRoom(owner);
    room.players.push(player);

    var playerData = {
        player: player,
        color: ""
    };
    room.playerData.push(playerData);
    // send new players in room to old players
    updateRoomForRoomPlayers(room, requestTypes.addToRoom);
    // send new roomData to all lobby players
    updateRoomsForLobbyPlayers();
}

function leaveFromRoom(owner, player, callback_id) {

    var room = searchRoom(owner);
    room.players.splice(room.players.indexOf(player), 1);

    for (var j = 0; j < room.playerData.length; j++) {
        // remove playerData from room
        if (room.playerData[ j ].player.indexOf(player) !== -1) {
            room.playerData.splice(j, 1);
        }
    }

    updateRoomForRoomPlayers(room, requestTypes.leaveFromRoom);

    // delete room, if owner left from room
    if (owner === player) {
        roomsList.splice(roomsList.indexOf(room), 1);
    }

    var requestUpdate = {
        callback_id: callback_id,
        requestType: requestTypes.getRooms,
        roomsList: roomsList
    };

    // update roomList for all players with lobby listener
    sendReqToPlayers(requestUpdate, lobbyPlayers);
}

function startGame(roomOwner, money, hits, wind, rounds, playerData) {
    roomsList.splice(roomsList.indexOf(searchRoom(roomOwner)), 1);

    var newGame = new game.Game(wind, hits, rounds);

    for (var i = 0; i < playerData.length; i++) {
        newGame.addPlayerData(playerData[ i ].player, playerData[ i ].color, money);

        // remove players from lobby listeners
        function searchPlayer(element, index, array)
        {
            if (element == playerData[ i ].player)
                return true;
        }

        lobbyPlayers.splice(lobbyPlayers.indexOf(lobbyPlayers.find(searchPlayer)), 1);
    }

    gamesList.push(newGame);


    var request = {
        callback_id: 'force',
        requestType: requestTypes.startGame,
        game: newGame
    };

    updateGamePlayers(newGame, request);
}

function addReadyPlayer(player, callback_id) {
    var game = searchPlayerGame(player);
    game.setReady(player, true);

    var request = {
        callback_id: 'force',
        requestType: requestTypes.readyPlayer,
        player: player
    };

    updateGamePlayers(game, request);
}

function PlayerMoveUpdate (game, player, move, callback_id) {
    var request = {
        callback_id: "force",
        requestType: requestTypes.playerMove,
        player : player,
        movementType : move
    };

    updateGamePlayers(game,request);
}

function changeTurn(game) {
    var request = {
        callback_id: "force",
        requestType: requestTypes.changeTurn
    };
    updateGamePlayers(game,request);
}

function changeWeapon (game,weapon)
{
    var request =
    {
        callback_id: "force",
        requestType: requestTypes.changeWeapon,
        weapon : weapon
    };
    updateGamePlayers(game,request);
}

// technical functions

function searchConnection(player) {
    function search(element, index, array) {
        if (element.user === player)
            return true;
    }

    return connections.find(search);
}

function searchRoom(owner) {
    function findRoom(el, index, array) {
        if (owner === el.owner)
            return true;
    }

    return roomsList.find(findRoom);
}

function searchPlayerGame(player) {
    function findGame(el, index, array) {
        for (var i = 0; i < el.playerData.length; i++) {
            if (player === el.playerData[ i ].player)
                return true;
        }
    }

    return gamesList.find(findGame);
}

function sendReqToPlayers(playersArray, request) {
    for (var i = 0; i < playersArray.length; i++) {
        try {
            var con = searchConnection(playersArray[ i ]);
            con.connection.send(JSON.stringify(request));
        }
        catch (Exception) {
            console.log("[ERROR] Error while sending request!");
            console.log("[ERROR] Exception:", Exception);
            console.log("[ERROR] Request:", JSON.stringify(request));
        }
    }
}

function updateGamePlayers(game, request) {
    try {
        for (var i = 0; i < game.playerData.length; i++) {
            function findGamePlayers(element, index, array)
            {
                if (game.playerData[ i ].player === element.user)
                    return true;
            }

            var con = connections.find(findGamePlayers);
            con.connection.send(JSON.stringify(request));

        }
    }
    catch (e) {
        console.log("[ERROR] Error while sending request!");
        console.log("[ERROR] Exception:", e);
        console.log("[ERROR] Request:", JSON.stringify(request));
    }
}

function updateRoomForRoomPlayers(room, reqType) {
    // update room for all people, that was in room
    for (var i = 0; i < room.players.length; i++) {
        function getRoomPlayer(element, index, array) {
            if (room.players[ i ] === element.user)
                return true;
        }

        var request = {
            callback_id: "force",
            requestType: reqType,

            players: room.players,
            playerData: room.playerData
        };

        try {
            var con = connections.find(getRoomPlayer);
            con.connection.send(JSON.stringify(request));
        }
        catch (Exception) {
            console.log("[ERROR] Error while sending request!");
            console.log("[ERROR] Exception:", Exception);
            console.log("[ERROR] Request:", JSON.stringify(request));
        }
    }
}

function updateRoomsForLobbyPlayers() {
    for (var i = 0; i < lobbyPlayers.length; i++) {
        function findLobbyPlayers(element, index, array) {
            if (lobbyPlayers[ i ] === element.user)
                return true;
        }

        var con = connections.find(findLobbyPlayers);

        getRooms(con.user, "force", con.connection);
    }
}

function updatePlayerData(owner, playerData, callback_id, ws) {
    var room = searchRoom(owner);
    room.playerData = playerData;

    updateRoomForRoomPlayers(room, requestTypes.updatePlayerData);
}